/*
** Image.cpp for  in /home/thepatriot/thepatriotsrepo/perso/codingclub
**
** Made by Alexis Bertholom
** Login   bertho_d
** Email   <alexis.bertholom@epitech.eu>
**
** Started on  Tue Jan 27 12:44:19 2015 Alexis Bertholom
// Last update Tue Nov 21 19:59:12 2017 Lucas
*/

#include "Image.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <cstring>
#include "SDLError.hpp"

Image::Image(Uint w, Uint h)
    : _img{},
      _allocd{true},
      _size{w * h},
      _texture_cache{},
      _font_cache{nullptr} {
  if (!(this->_img = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0)))
    throw(SDLError("SDL surface creation failed"));
  this->_size = w * h;
}

Image::Image(SDL_Surface* img, Image::Type type)
    : _img{}, _allocd{true}, _size{0}, _texture_cache{}, _font_cache{nullptr} {
  if (!img) throw(Error("Image: nullptr"));
  this->_size = img->w * img->h;
  if (type == Image::Wrap) {
    this->_allocd = false;
    this->_img = img;
    return;
  }
  this->_allocd = true;
  if (!(this->_img = SDL_CreateRGBSurface(0, img->w, img->h, sizeof(Color) << 3,
                                          0, 0, 0, 0)))
    throw(SDLError("SDL surface creation failed"));
  SDL_BlitSurface(img, NULL, this->_img, NULL);
}

Image::~Image() {
  if (this->_font_cache) {
    TTF_CloseFont(this->_font_cache);
  }
  if (this->_allocd) SDL_FreeSurface(this->_img);
}

void Image::putPixel(Uint x, Uint y, Color color) {
  if ((int)x < this->_img->w && (int)y < this->_img->h)
    *((Color*)this->_img->pixels + sizeof(Color) * (y * this->_img->w + x)) =
        color;
}

void Image::putRect(Uint x, Uint y, Uint w, Uint h, Color color) {
  SDL_Rect r;

  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_FillRect(this->_img, &r, color);
}

void		Image::putGrid(int tab[8][8], int x, int y)
{
  SDL_Rect	r;
  int		width = this->_img->w;
  int		height = this->_img->h;
  int		i = 0;
  int		j = 0;
  int		color;
  
  this->fill(0xa49381);
  r.w = width / x - 10;
  r.h = width / y - 10;
  while (j < y)
    {
      i = 0;
      r.y = height / y * j + 5;
      while (i < x)
	{
	  r.x = width / x * i + 5;
	  switch(tab[j][i])
	    {
	    case 0:
	      color = 0x00751D;
	      break;
	    case 1:
	      color = 0x000000;
	      break;
	    case 2:
	      color = 0xFFFFFF;
	      break;
	    default:
	      color = 0x0075D;
	      break;
	    }
	  SDL_FillRect(this->_img, &r, color);
	  i++;
	}
      j++;
    }
}

void Image::blit(Uint x, Uint y, Image& img, Uint w, Uint h) {
  SDL_Rect r;

  r.x = x;
  r.y = y;
  r.w = w;
  r.h = h;
  SDL_BlitSurface(img.getSurface(), &r, this->_img, NULL);
}

void Image::putText(std::string const& msg, Uint x, Uint y, Color color) {
  TTF_Font* font{this->_font_cache};
  if (!font) {
    font = TTF_OpenFont(IMAGE_FONT_PATH IMAGE_FONT_NAME, IMAGE_FONT_SIZE);
    if (!font) {
      throw SDLError("Couldn't load requested font: " IMAGE_FONT_NAME);
    }
    this->_font_cache = font;
  }
  SDL_Color render_color{static_cast<Uint8>((color >> 16) & 0xFF),
                         static_cast<Uint8>((color >> 8) & 0xFF),
                         static_cast<Uint8>(color & 0xFF),
                         static_cast<Uint8>(0xFF)};
  SDL_Surface* rendered{
      TTF_RenderText_Blended(font, msg.c_str(), render_color)};
  if (!rendered) {
    throw SDLError("Couldn't render text");
  }
  int width, height;
  if (TTF_SizeText(font, msg.c_str(), &width, &height)) {
    throw SDLError("Couldn't size text");
  }
  SDL_Rect dstrect{static_cast<int>(x), static_cast<int>(y),
                   static_cast<int>(width), static_cast<int>(height)};
  if (SDL_BlitSurface(rendered, NULL, this->_img, &dstrect) < 0) {
    throw SDLError("Couldn't blit texture");
  }
  SDL_FreeSurface(rendered);
}

void Image::putImage(std::string const& path, Uint x, Uint y) {
  SDL_Surface* texture{this->_texture_cache[path]};
  if (!texture) {
    texture = IMG_Load(path.c_str());
    if (!texture) {
      throw SDLError("Couldn't load requested file");
    }
    this->_texture_cache[path] = texture;
  }
  SDL_Rect dstrect{static_cast<int>(x), static_cast<int>(y), texture->w,
                   texture->h};
  if (SDL_BlitSurface(texture, NULL, this->_img, &dstrect) < 0) {
    throw SDLError("Couldn't blit texture");
  }
}

void Image::clear() {
  memset(this->_img->pixels, 0, this->_size * sizeof(Color));
}

void Image::fill(Color color) {
  Color* ptr = (Color*)this->_img->pixels;

  for (Uint i = 0; i < this->_size; ++i) *(ptr++) = color;
}

SDL_Surface* Image::getSurface() { return (this->_img); }
